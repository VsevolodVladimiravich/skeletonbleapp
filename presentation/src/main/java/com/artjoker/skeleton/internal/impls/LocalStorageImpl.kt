package com.artjoker.skeleton.internal.impls

import com.artjoker.core.gateways.LocalStorage
import com.artjoker.core.gateways.LocalStorage.ExpirableValue
import com.orhanobut.hawk.Hawk
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.reflect.KProperty

@Singleton
class LocalStorageImpl @Inject constructor() : LocalStorage {


    class StorageProperty<T>(
            private val defaultValue: T? = null,
            private val onPropertyChanged: (T) -> Unit = {}) {

        operator fun getValue(thisRef: Any, property: KProperty<*>): T =
                Hawk.get<T>(property.name, defaultValue)

        operator fun setValue(thisRef: Any, property: KProperty<*>, value: T) {
            Hawk.put(property.name, value)
            onPropertyChanged(value)
        }
    }


    class ExpirableProperty<T : Any>(
            private val defaultValue: T?,
            private val expiresAfterMillis: Long) {

        operator fun getValue(thisRef: Any, property: KProperty<*>): ExpirableValue<T> =
                ExpirableValue(
                        Hawk.get<T>(property.name, defaultValue),
                        isTimeExpired(property)
                )

        operator fun setValue(thisRef: Any, property: KProperty<*>, value: ExpirableValue<T>) {
            Hawk.put(property.name, value.value)
            fixChangeTime(property)
        }

        private fun fixChangeTime(property: KProperty<*>) {
            Hawk.put(timeStampKey(property), System.currentTimeMillis())
        }

        private fun isTimeExpired(property: KProperty<*>) =
                Hawk.get<Long>(timeStampKey(property), 0)
                        .let { timeStamp ->
                            System.currentTimeMillis() - timeStamp >= expiresAfterMillis
                        }

        private fun timeStampKey(property: KProperty<*>) = "${property.name}.timeStamp"
    }
}
