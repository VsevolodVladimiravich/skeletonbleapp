package com.artjoker.skeleton.internal.di

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import com.artjoker.core.gateways.BluetoothFacade
import com.artjoker.core.gateways.FirmwareGateway
import com.artjoker.core.gateways.LocalStorage
import com.artjoker.core.gateways.NetworkFacade
import com.artjoker.data.BluetoothFacadeImpl
import com.artjoker.data.FirmwareGatewayImpl
import com.artjoker.data.NetworkFacadeImpl
import com.artjoker.skeleton.Application
import com.artjoker.skeleton.internal.impls.LocalStorageImpl
import com.artjoker.skeleton.internal.impls.ToastManagerImpl
import com.artjoker.skeleton.internal.impls.ViewModelFactoryImpl
import com.artjoker.skeleton.ui.bases.BaseViewModel
import com.artjoker.skeleton.ui.screens.deviseslist.DevicesListFragment
import com.artjoker.skeleton.ui.screens.deviseslist.DevicesListViewModel
import com.artjoker.skeleton.ui.screens.enableBluetooth.BluetoothPromoDialogFragment
import com.artjoker.skeleton.ui.screens.initial.SplashActivity
import com.artjoker.skeleton.ui.screens.main.MainActivity
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideCicerone(): Cicerone<Router> = Cicerone.create()

    @Provides
    fun provideNavigatorHolder(cicerone: Cicerone<Router>): NavigatorHolder =
            cicerone.navigatorHolder

    @Provides
    fun provideRouter(cicerone: Cicerone<Router>): Router = cicerone.router
}


@Module
interface ImplementationsModule {
    @Binds fun context(application: Application): Context
    @Binds fun networkFacade(networkFacade: NetworkFacadeImpl): NetworkFacade
    @Binds fun bluetoothFacade(bluetoothFacadeImpl: BluetoothFacadeImpl): BluetoothFacade
    @Binds fun localStorage(LocalStorage: LocalStorageImpl): LocalStorage
    @Binds fun viewModelFactory(viewModelFactory: ViewModelFactoryImpl): ViewModelProvider.Factory
    @Binds fun toastManager(toastManager: ToastManagerImpl): BaseViewModel.ToastManager
    @Binds fun firmwareGateway(gateway: FirmwareGatewayImpl): FirmwareGateway
}


@Module
interface ActivitiesModule {
    @ContributesAndroidInjector fun splash(): SplashActivity
    @ContributesAndroidInjector fun main(): MainActivity
}


@Module
interface FragmentsModule {
    @ContributesAndroidInjector fun deviceList(): DevicesListFragment
    @ContributesAndroidInjector fun bluetothPromo(): BluetoothPromoDialogFragment
}


/**
 * Example:
 * @Binds
 * @IntoMap
 * @ViewModelKey(SignInViewModel::class)
 * fun signIn(viewModel: SignInViewModel): ViewModel
 */
@Module
interface ViewModelsModule {
    @Binds
    @IntoMap
    @ViewModelKey(DevicesListViewModel::class)
    fun deviceList(viewModel: DevicesListViewModel): ViewModel
}
