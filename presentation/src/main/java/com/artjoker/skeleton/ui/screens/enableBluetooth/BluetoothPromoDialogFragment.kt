package com.artjoker.skeleton.ui.screens.enableBluetooth

import com.artjoker.skeleton.R
import com.artjoker.skeleton.ui.Screens
import com.artjoker.skeleton.ui.bases.BaseBindingDialogFragment
import org.jetbrains.anko.support.v4.act

class BluetoothPromoDialogFragment : BaseBindingDialogFragment() {

    override val layoutId: Int = R.layout.dialog_fragment_enable_bluetooth

    fun quitClicked() {
        router.navigateTo(Screens.EXIT)
    }

    fun enableBluetooth() {
        router.replaceScreen(Screens.BLUETOOTH_ENABLE_SYSTEM)
    }
}