package com.artjoker.skeleton.ui.binding

import android.databinding.BindingConversion
import android.view.View
import com.artjoker.core.entity.Firmware
import com.artjoker.core.interactors.DeviceState

@BindingConversion
fun booleanToVisibility(visible: Boolean) = View.VISIBLE.takeIf { visible } ?: View.GONE

@BindingConversion
fun stateToOrdinal(state: DeviceState) = when (state) {
    is DeviceState.None -> 0
    is DeviceState.NoPermissions -> 1
    is DeviceState.BluetoothDisabled -> 2
    is DeviceState.ReadyToScan -> 3
    is DeviceState.Scanning -> 4
    is DeviceState.Connecting -> 5
    is DeviceState.Connected -> 6
    is DeviceState.Disconnected -> 7
    is DeviceState.UploadingFirmware -> 8
    is DeviceState.UploadingFinished -> 8
    is DeviceState.ReadyToUpdate -> 8
    is DeviceState.CancellingUploadingFirmware -> 9
    is DeviceState.FirmwareUploadSuccess -> 10
    is DeviceState.FirmwareUploadFail -> 11
}

@BindingConversion
fun firmwareToString(firmware: Firmware) = firmware.fileName