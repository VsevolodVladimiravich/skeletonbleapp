package com.artjoker.skeleton.ui.bases

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.content.IntentFilter
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.artjoker.core.gateways.LocalStorage
import com.artjoker.skeleton.R
import com.artjoker.skeleton.databinding.PreloaderBinding
import com.artjoker.skeleton.databinding.SwipeRefreshLayoutBinding
import com.artjoker.skeleton.internal.LifecycleLogger
import com.artjoker.skeleton.internal.Logger
import com.artjoker.skeleton.ui.AppEvents
import com.artjoker.skeleton.ui.Screens
import com.artjoker.skeleton.ui.utils.setViewModel
import com.artjoker.skeleton.ui.utils.withViewModel
import dagger.android.support.DaggerFragment
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import org.jetbrains.anko.support.v4.act
import org.jetbrains.anko.support.v4.ctx
import ru.terrakok.cicerone.Router
import rx2.receiver.android.RxReceiver
import java.lang.UnsupportedOperationException
import javax.inject.Inject

abstract class BaseFragment : DaggerFragment(), AppEvents.Listener {

    abstract val layoutId: Int

    protected open val titleId = R.string.app_name
    protected open val withUpButton = false

    @Inject lateinit var router: Router
    @Inject lateinit var events: AppEvents
    @Inject lateinit var storage: LocalStorage

    private val onCreateDisposables = CompositeDisposable()
    private val lifecycleLogger = LifecycleLogger(this.javaClass.simpleName)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycle.addObserver(lifecycleLogger)
        events.addListener(this)
    }

    override fun onDestroy() {
        events.removeListener(this)
        onCreateDisposables.dispose()
        super.onDestroy()
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View = inflater.inflate(layoutId, container, false)

    override fun onResume() {
        super.onResume()

        if (titleId != 0) {
            act.setTitle(titleId)
        }

        (act as AppCompatActivity).supportActionBar?.apply {
            setDisplayShowHomeEnabled(withUpButton)
            setDisplayHomeAsUpEnabled(withUpButton)
        }
    }

    open fun onPermissionGranted(permissions: Array<String>) {
        throw UnsupportedOperationException("onPermissionGranted")
    }

    open fun onPermissionNotGranted(permissions: Array<String>) {
        throw UnsupportedOperationException("onPermissionNotGranted")
    }

    open fun onBackPressed(): Boolean = false

    protected fun <T> Observable<T>.subscribeOnCreate(onNext: (T) -> Unit) {
        onCreateDisposables.add(subscribe(onNext))
    }

    protected fun registerBluetoothReceiver(): Observable<Intent> =
            RxReceiver.receives(ctx, IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED))

}


abstract class BindingFragment : BaseFragment() {

    protected open val viewModel: Any get() = this

    private val _bindings = mutableSetOf<ViewDataBinding>()
    protected val bindings: Set<ViewDataBinding> get() = _bindings

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ) = safeInflate<ViewDataBinding>(layoutId, inflater, container).root

    override fun onDestroyView() {
        super.onDestroyView()

        _bindings.forEach {
            try {
                it.unbind()
                it.setViewModel(null)
            } catch (ignored: RuntimeException) {
                ignored.printStackTrace()
            }
        }

        _bindings.clear()
    }

    protected fun <VDB : ViewDataBinding> safeInflate(
            layoutId: Int,
            inflater: LayoutInflater,
            viewGroup: ViewGroup? = null
    ): VDB = DataBindingUtil.inflate<VDB>(inflater, layoutId, viewGroup, false)
            .withViewModel(viewModel)
            .also { _bindings.add(it) }

    protected inline fun <reified VDB : ViewDataBinding> binding() =
            bindings.filterIsInstance<VDB>()
                    .first()

    protected inline fun <reified VDB : ViewDataBinding> withBinding(block: VDB.() -> Unit) {
        block(binding())
    }
}


abstract class ViewModelFragment<VM : BaseViewModel> : BindingFragment() {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    final override val viewModel by lazy(::obtainViewModel)

    private val useActivityScope = arguments?.getBoolean(ARG_USE_ACTIVITY_SCOPE) ?: false

    protected abstract val viewModelClass: Class<VM>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState ?: (viewModel as? WithRefresh)?.refresh()
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View = super.onCreateView(inflater, container, savedInstanceState)
            .let { view ->
                when (viewModel) {
                    is WithRefresh -> view.wrapBy(
                            inflater,
                            R.layout.swipe_refresh_layout,
                            SwipeRefreshLayoutBinding::swipeRefreshLayout
                    )
                    else -> view
                }
            }
            .wrapBy(inflater, R.layout.preloader, PreloaderBinding::viewSwitcherContent)

    private fun <VDB : ViewDataBinding> View.wrapBy(
            inflater: LayoutInflater,
            layoutId: Int,
            getViewGroup: VDB.() -> ViewGroup
    ): View = safeInflate<VDB>(layoutId, inflater)
            .apply {
                getViewGroup(this).addView(this@wrapBy)
            }
            .root

    private fun obtainViewModel() = when {
        useActivityScope -> ViewModelProviders.of(act as FragmentActivity, viewModelFactory)
        else -> ViewModelProviders.of(this, viewModelFactory)
    }.get(viewModelClass)

    final override fun onBackPressed() = viewModel.onBackPressed()

    final override fun onPermissionGranted(permissions: Array<String>) {
        viewModel.onPermissionGranted(permissions)
    }

    final override fun onPermissionNotGranted(permissions: Array<String>) {
        viewModel.onPermissionNotGranted(permissions)
    }


    companion object {
        const val ARG_USE_ACTIVITY_SCOPE = "ARG_USE_ACTIVITY_SCOPE"
    }
}
