package com.artjoker.skeleton.ui.bases

import android.app.Dialog
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.View
import com.artjoker.skeleton.internal.LifecycleLogger
import com.artjoker.skeleton.ui.utils.withViewModel
import dagger.android.support.DaggerAppCompatDialogFragment
import org.jetbrains.anko.layoutInflater
import org.jetbrains.anko.support.v4.ctx
import ru.terrakok.cicerone.Router
import javax.inject.Inject

abstract class BaseDialogFragment : DaggerAppCompatDialogFragment() {

    protected open val cancelable get() = true

    @Inject lateinit var router: Router

    protected abstract fun onCreateAlertView(): View

    private val lifecycleLogger = LifecycleLogger(this.javaClass.simpleName)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycle.addObserver(lifecycleLogger)
        isCancelable = cancelable
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
            AlertDialog.Builder(ctx)
                    .setView(onCreateAlertView())
                    .create()
}


abstract class BaseBindingDialogFragment : BaseDialogFragment() {

    protected open val viewModel: Any get() = this

    abstract val layoutId: Int

    final override fun onCreateAlertView(): View =
            DataBindingUtil
                    .inflate<ViewDataBinding>(ctx.layoutInflater, layoutId, null, false)
                    .withViewModel(viewModel)
                    .root
}
