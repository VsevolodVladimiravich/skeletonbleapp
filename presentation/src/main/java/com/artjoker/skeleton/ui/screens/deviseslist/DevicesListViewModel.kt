package com.artjoker.skeleton.ui.screens.deviseslist

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.databinding.ObservableField
import android.view.View
import com.artjoker.core.entity.BluetoothDevice
import com.artjoker.core.entity.Firmware
import com.artjoker.core.interactors.ConnectedDeviceInteractor
import com.artjoker.core.interactors.DeviceState
import com.artjoker.core.interactors.NativeDeviceUpdate
import com.artjoker.core.interactors.NativeDeviceUpdates
import com.artjoker.skeleton.R
import com.artjoker.skeleton.internal.Logger.e
import com.artjoker.skeleton.ui.Screens
import com.artjoker.skeleton.ui.bases.ListViewModel
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class DevicesListViewModel @Inject constructor(
        private val connectedDeviceInteractor: ConnectedDeviceInteractor
) :
        ListViewModel<BluetoothDevice>(),
        NativeDeviceUpdates {

    val deviceState = ObservableField<DeviceState>(DeviceState.None)

    private lateinit var updates: PublishSubject<NativeDeviceUpdate>
    private lateinit var firmware: Firmware

    override fun updates(): Observable<NativeDeviceUpdate> = updates
    override fun onCreateRecyclerViewAdapter(): BaseAdapter = Adapter()
    override fun onListChange(old: List<BluetoothDevice>, new: List<BluetoothDevice>): BaseDiffUtilCallback = DiffUtilCallback(old, new)

    @Inject
    fun init() {
        updates = PublishSubject.create<NativeDeviceUpdate>()
        connectedDeviceInteractor.stateChanges(this)
                .execute {
                    onNext {
                        e(this, it.name)
                        deviceState.set(it)
                        when (it) {
                            is DeviceState.Scanning -> addIfNotAdded(it)
                            is DeviceState.ReadyToUpdate -> updates.onNext(NativeDeviceUpdate.ReadyToUpdate(firmware))
                            is DeviceState.UploadingFinished -> updates.onNext(NativeDeviceUpdate.FinishUpdate)
                        }
                    }

                    onComplete { e(this, "onComplete") }

                    onError { deviceState.set(DeviceState.Disconnected) }
                }
    }

    override fun onPermissionNotGranted(permissions: Array<String>) {
        e(this, "onPermissionNotGranted")
        updates.onNext(NativeDeviceUpdate.PermissionNotGranted)
    }

    override fun onPermissionGranted(permissions: Array<String>) {
        e(this, "onPermissionGranted")
        updates.onNext(NativeDeviceUpdate.PermissionGranted)
    }

    override fun onBluetoothEnabled() {
        e(this, "onBluetoothEnabled")
        updates.onNext(NativeDeviceUpdate.BluetoothEnabled)
    }

    fun processAccessCoarsePermission(granted: Boolean) {
        e(this, "processAccessCoarsePermission")
        when {
            granted && deviceState.get() == DeviceState.None -> updates.onNext(NativeDeviceUpdate.PermissionGranted)
            !granted -> updates.onNext(NativeDeviceUpdate.PermissionNotGranted)
        }
    }

    fun requestPermissions() {
        e(this, "requestPermissions")
        router.navigateTo(Screens.PERMISSIONS, arrayOf(ACCESS_COARSE_LOCATION))
    }

    fun enableBluetooth() {
        e(this, "enableBluetooth")
        router.navigateTo(Screens.BLUETOOTH_ENABLE_SYSTEM)
    }

    fun reconnectDevice() {
        e(this, "reconnectDevice")
        init()
        updates.onNext(NativeDeviceUpdate.Reconnect)
    }

    fun startScan() {
        e(this, "startScan")
        updates.onNext(NativeDeviceUpdate.StartScan)
    }

    fun firmwareClicked(firmware: Firmware) {
        e(this, "firmwareClicked: $firmware")
        this.firmware = firmware
        updates.onNext(NativeDeviceUpdate.PrepareFirmwareUpdate)
    }

    fun enableDevice() {
        e(this, "enableDevice")
        updates.onNext(NativeDeviceUpdate.EnableDevice)
    }

    fun disableDevice() {
        e(this, "disableDevice")
        updates.onNext(NativeDeviceUpdate.DisableDevice)
    }

    private fun addIfNotAdded(state: DeviceState.Scanning) {
        state.device?.let { device ->
            mutateList { list ->
                e(this, device.toString())
                if (device !in list) list.add(0, device)
            }
        }
    }


    private inner class DiffUtilCallback(
            override val old: List<BluetoothDevice>,
            override val new: List<BluetoothDevice>
    ) : BaseDiffUtilCallback() {

        override fun areItemsTheSame(i1: BluetoothDevice, i2: BluetoothDevice): Boolean =
                i1.macAddress == i2.macAddress

        override fun areContentsTheSame(i1: BluetoothDevice, i2: BluetoothDevice): Boolean =
                i1.name == i2.name
    }


    private inner class Adapter : BaseAdapter() {

        override val itemLayoutId = R.layout.item_bluetooth_device
        override fun onCreateItemViewHolder(v: View): ItemViewHolder = ViewHolder(v)
    }


    inner class ViewHolder constructor(view: View) : ItemViewHolder(view) {

        fun onClick() {
            updates.onNext(NativeDeviceUpdate.Connect(item.get()!!.macAddress))
        }
    }
}