package com.artjoker.skeleton.ui.screens.deviseslist

import android.Manifest
import com.artjoker.skeleton.R
import com.artjoker.skeleton.ui.bases.ViewModelFragment
import com.tbruyelle.rxpermissions2.RxPermissions
import org.jetbrains.anko.support.v4.act

class DevicesListFragment : ViewModelFragment<DevicesListViewModel>() {

    override val layoutId = R.layout.fragment_devices_list
    override val viewModelClass = DevicesListViewModel::class.java

    override fun onResume() {
        super.onResume()

        val granted = RxPermissions(act).isGranted(Manifest.permission.ACCESS_COARSE_LOCATION)
        viewModel.processAccessCoarsePermission(granted)
    }
}
