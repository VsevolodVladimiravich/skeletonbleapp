package com.artjoker.skeleton.ui.utils

import android.databinding.ObservableInt
import android.databinding.ViewDataBinding
import com.artjoker.skeleton.BR

fun <VDB : ViewDataBinding> VDB.withViewModel(viewModel: Any?) = apply {
    if (!setVariable(BR.viewModel, viewModel)) {
        throw IllegalStateException()
    }
}


fun ViewDataBinding.setViewModel(viewModel: Any?) {
    withViewModel(viewModel)
}

fun ObservableInt.dec() = set(get() - 1)

fun ObservableInt.inc() = set(get() + 1)
