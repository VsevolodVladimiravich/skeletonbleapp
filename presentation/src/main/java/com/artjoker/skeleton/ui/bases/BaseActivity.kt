package com.artjoker.skeleton.ui.bases

import android.app.Activity
import android.app.DatePickerDialog
import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentFilter
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.LinearLayout
import com.artjoker.core.gateways.LocalStorage
import com.artjoker.skeleton.BuildConfig
import com.artjoker.skeleton.R
import com.artjoker.skeleton.databinding.ToolbarBinding
import com.artjoker.skeleton.internal.LifecycleLogger
import com.artjoker.skeleton.ui.AppEvents
import com.artjoker.skeleton.ui.Screens
import com.artjoker.skeleton.ui.bases.pickers.DatePicker
import com.artjoker.skeleton.ui.screens.deviseslist.DevicesListFragment
import com.artjoker.skeleton.ui.screens.enableBluetooth.BluetoothPromoDialogFragment
import com.artjoker.skeleton.ui.screens.main.MainActivity
import com.artjoker.skeleton.ui.utils.*
import com.tbruyelle.rxpermissions2.RxPermissions
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import org.jetbrains.anko.*
import org.joda.time.LocalDate
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.SupportAppNavigator
import ru.terrakok.cicerone.commands.Back
import ru.terrakok.cicerone.commands.Command
import rx2.receiver.android.RxReceiver
import javax.inject.Inject

abstract class BaseActivity : DaggerAppCompatActivity(), AppEvents.Listener {

    @Inject lateinit var router: Router
    @Inject lateinit var navigatorHolder: NavigatorHolder

    @Inject lateinit var storage: LocalStorage
    @Inject lateinit var events: AppEvents

    protected open val layoutId = 0
    protected open val navigator: Navigator = BaseNavigator()
    protected open val setupToolbarAction: SetupToolbarAction? = null

    private var alert: DialogInterface? = null
    private val lifecycleLogger = LifecycleLogger(this.javaClass.simpleName)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycle.addObserver(lifecycleLogger)
        events.addListener(this)
        layoutId.takeIf { it != 0 }?.let(::setContentView)
    }

    override fun onDestroy() {
        super.onDestroy()
        events.removeListener(this)
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun onBackPressed() {
        tryToFind<Fragment>().let { fragment ->
            if (fragment !is BaseFragment || !fragment.onBackPressed()) {
                router.exit()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when {
            (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_OK) -> {
                events.notifyListeners { onBluetoothEnabled() }
            }
            (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) -> {
                //todo handle cancel
            }
        }
    }

    protected open fun onNavCommandApplied(command: Command) {}

    protected open fun currentFragment(): Fragment? =
            supportFragmentManager.findFragmentById(R.id.fragment_container)

    protected inline fun <reified F> currentFragment(): F = currentFragment() as F

    protected inline fun <reified F : Fragment> tryToFind(): F? =
            supportFragmentManager.fragments.find { it is F } as? F

    protected inline fun <reified F : Fragment> find(): F = tryToFind()!!

    protected fun hideKeyboard() {
        val fragmentRoot = currentFragment()
                ?.view
                ?.rootView

        (fragmentRoot ?: currentFocus)
                ?.let { inputMethodManager.hideSoftInputFromWindow(it.windowToken, 0) }
    }

    protected fun registerBluetoothReceiver(): Observable<Intent> =
            RxReceiver.receives(ctx, IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED))

    protected fun showKeyboard(editText: EditText) {
        inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
    }

    protected fun AlertBuilder<*>.replace() {
        alert?.dismiss()
        alert = show()
    }

    protected fun DialogFragment.show() {
        show(supportFragmentManager, javaClass.name)
    }

    protected inline fun <reified D : DialogFragment> dismissIfShown() =
            tryToFind<D>()?.dismiss()

    companion object {
        private const val REQUEST_ENABLE_BT: Int = 214
    }


    protected open inner class BaseNavigator : SupportAppNavigator(this, R.id.fragment_container) {

        override fun applyCommand(command: Command) {
            if (command is Back) {

                dismissIfShown<DialogFragment>() ?: super.applyCommand(command)

            } else {

                when (command.screenKey()) {
                    Screens.BLUETOOTH_ENABLE_SYSTEM -> {
                        val intent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                        startActivityForResult(intent, REQUEST_ENABLE_BT)
                    }

                    Screens.BLUETOOTH_PROMO -> BluetoothPromoDialogFragment().show()

                    Screens.ALERT -> command.transitionData<Any>().let { data ->
                        when (data) {
                            is Int -> showSimpleAlert(data)
                            is String -> showSimpleAlert(data)
                            else -> throw IllegalAccessException()
                        }
                    }

                    Screens.ERROR -> showSimpleAlert(
                            command.transitionData<Throwable>().message
                                    ?: getString(R.string.error_unknown)
                    )

                    Screens.PROGRESS,
                    Screens.CANCELABLE_PROGRESS -> showProgress(
                            disposable = command.transitionData() as Disposable,
                            cancelable = command.screenKey() == Screens.CANCELABLE_PROGRESS
                    )

                    Screens.DATE_PICKER -> {

                        val viewModel = command.transitionData<DatePicker>()
                        val now = LocalDate.now()

                        DatePickerDialog(
                                this@BaseActivity,
                                DatePickerDialog.OnDateSetListener { _, y, m, d ->
                                    viewModel.notifyDatePicked(LocalDate(y, m + 1, d))
                                },
                                now.year,
                                now.monthOfYear - 1,
                                now.dayOfMonth
                        ).apply {
                            viewModel.maxDate?.toDateTimeAtStartOfDay()?.millis?.let(datePicker::setMaxDate)
                            viewModel.minDate?.toDateTimeAtStartOfDay()?.millis?.let(datePicker::setMinDate)
                        }.show()
                    }

                    Screens.EXIT -> finishAndRemoveTask()
                    Screens.PERMISSIONS -> {
                        RxPermissions(this@BaseActivity)
                                .request(*command.transitionData<Array<String>>())
                                .subscribe { granted ->
                                    when {
                                        granted -> BaseFragment::onPermissionGranted
                                        else -> BaseFragment::onPermissionNotGranted
                                    }.invoke(find(), command.transitionData())
                                }
                    }

                    else -> super.applyCommand(command)
                }
            }

            onNavCommandApplied(command)
        }

        override fun createActivityIntent(context: Context, screenKey: String, data: Any?) =
                when (screenKey) {
                    Screens.MAIN -> intentFor<MainActivity>()
                    else -> null
                }

        override fun createFragment(screenKey: String, data: Any?): Fragment? =
                when (screenKey) {
                    Screens.DEVICE_LIST -> DevicesListFragment()
                    else -> null
                }

        override fun unknownScreen(command: Command) {
            if (BuildConfig.DEBUG) {
                alert("Can't create a screen for passed screenKey: ${command.screenKey()}")
                        .show()
            }
        }
    }
}


abstract class BaseBindingActivity<VDB : ViewDataBinding> : BaseActivity() {

    protected val binding by lazy { DataBindingUtil.bind<VDB>(contentView!!)!! }

    protected open val viewModel: Any get() = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.setViewModel(viewModel)
    }
}


class SetupToolbarAction(
        val titleRes: Int = R.string.app_name,
        private val onNavClick: (() -> Unit)? = null
) {

    private lateinit var router: Router

    fun perform(activity: BaseActivity) {
        router = activity.router

        val toolbarBinding =
                ToolbarBinding.inflate(activity.layoutInflater).withViewModel(this@SetupToolbarAction)


        activity.linearLayout {
            orientation = LinearLayout.VERTICAL

            addView(toolbarBinding.root)

            val windowContent = activity.findViewById<ViewGroup>(android.R.id.content)
            val contentView = windowContent.getChildAt(0)
            windowContent.removeViewAt(0)

            addView(contentView)

        }.let(activity::setContentView)

        when (titleRes) {
            R.string.app_name -> activity.setSupportActionBar(toolbarBinding.toolbar)
            else -> toolbarBinding.toolbar.setTitle(titleRes)
        }
    }

    fun onNavigationClick() {
        onNavClick?.invoke() ?: router.exit()
    }
}
