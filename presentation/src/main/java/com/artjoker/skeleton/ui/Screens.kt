package com.artjoker.skeleton.ui

object Screens {

    const val MAIN = "MAIN"

    const val DEVICE_LIST = "DEVICE_LIST"

    const val BLUETOOTH_PROMO = "BLUETOOTH_PROMO"
    const val BLUETOOTH_ENABLE_SYSTEM = "BLUETOOTH_ENABLE_SYSTEM"

    const val ERROR = "ERROR"
    const val ALERT = "ALERT"
    const val PROGRESS = "PROGRESS"
    const val CANCELABLE_PROGRESS = "CANCELABLE_PROGRESS"
    const val DATE_PICKER = "DATE_PICKER"
    const val EXIT = "EXIT"

    const val PERMISSIONS = "PERMISSIONS"
}
