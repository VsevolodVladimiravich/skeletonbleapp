package com.artjoker.skeleton.ui.binding

import android.databinding.BindingAdapter
import android.widget.TextView
import com.artjoker.core.entity.EnabledState
import com.artjoker.core.entity.FirmwareType
import com.artjoker.core.entity.PlacementState.BROWSE_AIR
import com.artjoker.core.entity.PlacementState.BROWSE_HAND
import com.artjoker.core.entity.PlacementState.CANT_DEFINE_HART_RATE
import com.artjoker.core.entity.PlacementState.CHARGING
import com.artjoker.core.entity.PlacementState.DEFINITION_SURFACE
import com.artjoker.core.entity.PlacementState.NOT_A_HAND
import com.artjoker.core.entity.PlacementState.OVERLOADED
import com.artjoker.core.entity.PlacementState.RECEIVING_INFO
import com.artjoker.core.entity.PlacementState.TOO_ACTIVE
import com.artjoker.core.interactors.DeviceState
import com.artjoker.skeleton.R


@BindingAdapter("optionalText")
fun TextView.setOptionalText(textRes: Int) {
    text = when (textRes) {
        0 -> null
        else -> context.getString(textRes)
    }
}

@BindingAdapter("deviceState")
fun TextView.setDeviceState(state: DeviceState?) {
    when (state) {
        is DeviceState.Connected -> when (state.deviceEnabled) {
            EnabledState.ENABLE -> R.string.enabled
            EnabledState.DISABLE -> R.string.disabled
            EnabledState.RECEIVING_INFO -> R.string.sensor_receiving_info
            else -> R.string.sensor_unknown_value
        }.let(this::setText)
    }
}

@BindingAdapter("manufacturerData")
fun TextView.setManufacturerData(state: DeviceState?) {
    when (state) {
        is DeviceState.Connecting -> text = state.macAddress
    }
}

@BindingAdapter("sensorState")
fun TextView.setSensorState(state: DeviceState?) {
    when (state) {
        is DeviceState.Connected -> when (state.placementState) {
            BROWSE_AIR -> R.string.sensor_browsing_air
            DEFINITION_SURFACE -> R.string.sensor_definition_surface
            BROWSE_HAND -> R.string.sensor_browse_hand
            NOT_A_HAND -> R.string.sensor_not_a_hand
            CHARGING -> R.string.sensor_charging
            CANT_DEFINE_HART_RATE -> R.string.sensor_define_hart_rate
            TOO_ACTIVE -> R.string.sensor_too_active
            RECEIVING_INFO -> R.string.sensor_receiving_info
            OVERLOADED -> R.string.sensor_overloaded
            else -> R.string.sensor_unknown_value
        }.let(this::setText)
    }
}

@BindingAdapter("firmwareType")
fun TextView.setFirmwareType(state: DeviceState?) {
    when (state) {
        is DeviceState.Connected -> when (state.firmware) {
            FirmwareType.RECEIVING_INFO -> context.getString(R.string.sensor_receiving_info)
            else -> context.getString(R.string.firmware_type, state.firmware)
        }.let(this::setText)
    }
}