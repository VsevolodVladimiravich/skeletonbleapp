package com.artjoker.skeleton.ui.screens.main

import android.os.Bundle
import com.artjoker.skeleton.R
import com.artjoker.skeleton.ui.Screens
import com.artjoker.skeleton.ui.bases.BaseActivity
import com.artjoker.skeleton.ui.bases.SetupToolbarAction
import ru.terrakok.cicerone.Navigator

class MainActivity : BaseActivity() {

    override val layoutId = R.layout.fragment_container
    override val setupToolbarAction = SetupToolbarAction()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState ?: router.replaceScreen(Screens.DEVICE_LIST)
    }
}
