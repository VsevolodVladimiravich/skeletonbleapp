package com.artjoker.skeleton.ui.screens.initial

import android.os.Bundle
import com.artjoker.skeleton.ui.Screens
import com.artjoker.skeleton.ui.bases.BaseActivity

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        router.replaceScreen(Screens.MAIN)
    }
}
