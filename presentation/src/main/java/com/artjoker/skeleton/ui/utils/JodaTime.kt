package com.artjoker.skeleton.ui.utils

import org.joda.time.DateTime
import org.joda.time.LocalDate
import org.joda.time.format.DateTimeFormat

fun DateTime?.printByStyle(style: String): String =
        this?.let(DateTimeFormat.forStyle(style)::print) ?: ""

fun LocalDate?.printByStyle(style: String): String =
        this?.let(DateTimeFormat.forStyle(style)::print) ?: ""
