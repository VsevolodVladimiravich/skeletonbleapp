package com.artjoker.data.internal.di

import com.artjoker.data.internal.Api
import com.artjoker.data.internal.HeaderHolder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@Module
class ApiModule {

    @Provides
    fun provideApi(retrofit: Retrofit): Api = retrofit.create(Api::class.java)

    @Provides
    fun provideRetrofitBuilder(client: OkHttpClient): Retrofit =
            Retrofit.Builder()
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .baseUrl(API)
                    .build()

    @Provides
    fun provideOkHttpClient(logger: HttpLoggingInterceptor): OkHttpClient =
            OkHttpClient.Builder()
                    .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                    .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
                    .addInterceptor(logger)
                    .addInterceptor {

                        it.request().newBuilder().apply {

                            HeaderHolder.values().forEach { addHeader(it.name, it.header) }

                        }.build().let(it::proceed)
                    }
                    .build()

    @Provides
    fun provideLoggingInterceptor() =
            HttpLoggingInterceptor()
                    .apply { level = HttpLoggingInterceptor.Level.BODY }

    companion object {

        const val TIMEOUT = 25L

        const val API = "https://example.com/"
    }
}
