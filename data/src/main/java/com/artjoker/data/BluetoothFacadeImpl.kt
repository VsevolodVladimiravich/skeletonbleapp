package com.artjoker.data

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.Context
import android.os.ParcelUuid
import android.util.Log
import com.artjoker.core.entity.BluetoothDevice
import com.artjoker.core.entity.Commands
import com.artjoker.core.gateways.BluetoothFacade
import com.artjoker.core.interactors.BluetoothUpdate
import com.polidea.rxandroidble2.RxBleClient
import com.polidea.rxandroidble2.RxBleConnection
import com.polidea.rxandroidble2.internal.RxBleLog
import com.polidea.rxandroidble2.scan.ScanFilter
import com.polidea.rxandroidble2.scan.ScanResult
import com.polidea.rxandroidble2.scan.ScanSettings
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function3
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class BluetoothFacadeImpl @Inject constructor(
        private val context: Context
) : BluetoothFacade {

    override val enabled get() = bluetoothAdapter?.isEnabled ?: false

    private lateinit var rxBleClient: RxBleClient
    private lateinit var rxBleConnection: RxBleConnection
    private lateinit var macAddress: String
    private lateinit var rxObservable: Observable<ByteArray>
    private val bluetoothAdapter: BluetoothAdapter? by lazy(LazyThreadSafetyMode.NONE) {
        (context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager).adapter
    }
    private val scanSettings by lazy { ScanSettings.Builder().build() }
    private val scanFilterByUUID by lazy {
        ScanFilter.Builder().setServiceUuid(UUIDS.Service.main).build()
    }
    private val scanFilterByMacAddress by lazy {
        ScanFilter.Builder().setDeviceAddress(macAddress).build()
    }

    @Inject
    fun init() {
        rxBleClient = RxBleClient.create(context)
        RxBleClient.setLogLevel(RxBleLog.ERROR)
    }

    override fun startScan(): Observable<BluetoothUpdate> = logObservable("startScan")
            .flatMap { scan(scanFilterByUUID) }

    override fun connect(macAddress: String): Observable<BluetoothUpdate> = logObservable("connect macaddress:$macAddress")
            .flatMap { establishConnection(macAddress) }
            .map { this.macAddress = macAddress }
            .map { BluetoothUpdate.Connected }

    override fun reconnect(): Observable<BluetoothUpdate> = logObservable("reconnect")
            .flatMap { scan(scanFilterByMacAddress) }
            .map { it.device.macAddress }
            .flatMap(::establishConnection)
            .flatMapSingle { it.discoverServices() }
            .map { BluetoothUpdate.Connected }

    override fun setUpNotifications(): Observable<Observable<ByteArray>> = logObservable("setUpNotifications")
            .flatMap { rxBleConnection.setupNotification(UUIDS.Characteristic.rx) }
            .map { rxObservable = it; it }

    override fun sendReady(): Single<String> = logSingle("sendReady")
            .flatMap { writeTxCommand(Commands.TX.READY) }

    override fun sendDelete(): Single<String> = logSingle("sendDelete")
            .flatMap { writeTxCommand(Commands.TX.DELETE) }

    override fun uploadFirmware(firmware: List<ByteArray>): Observable<BluetoothUpdate.UploadingFirmware> =
            logObservable("uploadFirmware")
                    .flatMap { writeFirmware(firmware) }

    override fun sendEnd(): Observable<String> = logObservable("sendEnd")
            .concatMap { Observable.timer(2, TimeUnit.SECONDS) }
            .concatMap { establishConnection(macAddress) }
            .concatMap { setUpNotifications() }
            .flatMapSingle { writeTxCommand(Commands.TX.END) }

    override fun readCharacteristics(): Observable<BluetoothUpdate.Characteristics> = logObservable("readCharacteristics")
            .flatMapSingle {
                Single.zip(
                        rxBleConnection.readCharacteristic(UUIDS.Characteristic.placementState),
                        rxBleConnection.readCharacteristic(UUIDS.Characteristic.conditionState),
                        rxBleConnection.readCharacteristic(UUIDS.Characteristic.firmware),
                        Function3 { placement: ByteArray, condition: ByteArray, firmware: ByteArray ->
                            BluetoothUpdate.Characteristics(condition.value, placement.value, firmware.value)
                        })
            }

    override fun enableDevice(): Observable<BluetoothUpdate.Characteristics> = logObservable("enableDevice")
            .flatMap { writeConditionState(Commands.SWITCH.ON) }

    override fun disableDevice(): Observable<BluetoothUpdate.Characteristics> = logObservable("disableDevice")
            .flatMap { writeConditionState(Commands.SWITCH.OFF) }

    override fun enableFirmwareState():
            Observable<BluetoothUpdate> = logObservable("enableFirmwareState")
            .concatMap { establishConnection(macAddress) }
            .flatMapSingle { it.writeCharacteristic(UUIDS.Characteristic.reboot, Commands.REBOOT.FIRMWARE_UPDATE) }
            .map { BluetoothUpdate.FirmwareState }

    private fun establishConnection(macAddress: String): Observable<RxBleConnection> =
            rxBleClient.getBleDevice(macAddress)
                    .establishConnection(false)
                    .map { rxBleConnection = it;it }

    private fun writeFirmware(pages: List<ByteArray>): Observable<BluetoothUpdate.UploadingFirmware> =
            logObservable("writeFirmware")
                    .flatMap { Observable.fromIterable(pages) }
                    .concatMapSingle { longWriteWithResponse(it, pages) }

    private fun longWriteWithResponse(data: ByteArray, pages: List<ByteArray>): Single<BluetoothUpdate.UploadingFirmware> = Single.zip(
            longTXWrite(data).lastOrError(),
            responseRX(),
            BiFunction { write: ByteArray, read: ByteArray -> write.toLongWriteResponse(pages) }
    )

    private fun longTXWrite(page: ByteArray): Observable<ByteArray> = logObservable("longTXWrite page: ${page.contentToString()}")
            .flatMap {
                rxBleConnection.createNewLongWriteBuilder()
                        .setBytes(page)
                        .setMaxBatchSize(20)
                        .setCharacteristicUuid(UUIDS.Characteristic.tx)
                        .build()
            }

    private fun writeTxCommand(command: ByteArray): Single<String> =
            logCommand("writeTxCommand", command)
                    .flatMap {
                        Single.zip(
                                rxBleConnection.writeCharacteristic(UUIDS.Characteristic.tx, command),
                                responseRX(),
                                BiFunction { write: ByteArray, read: ByteArray -> read.response }
                        )
                    }

    private fun responseRX(): Single<ByteArray> = rxObservable
            .take(3)
            .map { it[0] }
            .toList()
            .map { it.toByteArray() }

    private fun writeConditionState(command: ByteArray): Observable<BluetoothUpdate.Characteristics> = logObservable("writeConditionState")
            .concatMap { establishConnection(macAddress) }
            .flatMapSingle { it.writeCharacteristic(UUIDS.Characteristic.conditionState, command) }
            .flatMap { logObservable("onCharWrite: ${it.contentToString()}") }
            .flatMap { readCharacteristics() }

    private fun scan(filter: ScanFilter): Observable<BluetoothUpdate.ScanResult> = logObservable("scan")
            .flatMap { rxBleClient.scanBleDevices(scanSettings, filter) }
            .map { it.bluetoothDevice }
            .map(BluetoothUpdate::ScanResult)


    private object UUIDS {


        object Characteristic {
            val firmware: UUID = UUID.fromString("00002a28-0000-1000-8000-00805f9b34fb")
            val placementState: UUID = UUID.fromString("0000ffe4-0000-1000-8000-00805f9b34fb")
            val conditionState: UUID = UUID.fromString("0000ff51-0000-1000-8000-00805f9b34fb")
            val reboot: UUID = UUID.fromString("0000ffc1-0000-1000-8000-00805f9b34fb")
            val rx: UUID = UUID.fromString("0000fff1-0000-1000-8000-00805f9b34fb")
            val tx: UUID = UUID.fromString("0000fff2-0000-1000-8000-00805f9b34fb")
        }


        object Service {
            val main: ParcelUuid = ParcelUuid(UUID.fromString("0000ffe0-0000-1000-8000-00805f9b34fb"))
        }
    }
}


private fun Any.logSingle(message: String?): Single<Any> = Single.just(loge(message))
private fun Any.logObservable(message: String?): Observable<Any> = Observable.just(loge(message))
private fun Any.logCommand(message: String?, command: ByteArray) = logSingle("${message.orEmpty()}, command: ${command.command}")
private fun Any.loge(message: String?) = Log.e("RxLog", message.orEmpty())

private val ByteArray.value get() = this[0].toInt()

private val ScanResult.bluetoothDevice
    get() = BluetoothDevice(this.bleDevice.name, this.bleDevice.macAddress)

private val ByteArray.response
    get() = when {
        this contentEquals Commands.RX.ACK -> Commands.RESPONSE.ACK
        this contentEquals Commands.RX.NACK -> Commands.RESPONSE.NACK
        else -> this.contentToString()
    }

private fun ByteArray.toLongWriteResponse(pages: List<ByteArray>) = BluetoothUpdate.UploadingFirmware(
        ((65 / pages.size) * pages.indexOf(this)) + 30,
        pages.size - 1 == pages.indexOf(this)
)

private val ByteArray.command
    get() = when {
        this contentEquals Commands.TX.END -> "END"
        this contentEquals Commands.TX.DELETE -> "DELETE"
        this contentEquals Commands.TX.READY -> "READY"
        else -> this.contentToString()
    }