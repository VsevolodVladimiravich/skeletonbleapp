package com.artjoker.data

import android.content.Context
import com.artjoker.core.entity.Firmware
import com.artjoker.core.gateways.FirmwareGateway
import io.reactivex.Single
import javax.inject.Inject

class FirmwareGatewayImpl @Inject constructor(
        private val context: Context
) : FirmwareGateway {

    override fun getFirmware(firmware: Firmware): Single<List<ByteArray>> = Single.create {
        var read = 0
        val chunks = arrayListOf(ByteArray(CHUNK_SIZE))

        try {
            val fileInputStream = context.assets.open(firmware.fileName)

            while (read != -1) {
                read = fileInputStream.read(chunks[chunks.size - 1], 0, CHUNK_SIZE)
                when (read) {
                    CHUNK_SIZE -> chunks.add(ByteArray(CHUNK_SIZE))
                }
            }

            fileInputStream.close()

        } catch (e: Exception) {
            it.onError(e)
        }

        chunks.removeAt(chunks.size - 1) //removing empty array

        it.onSuccess(chunks)
    }

    private companion object {
        const val CHUNK_SIZE = 2057
    }
}