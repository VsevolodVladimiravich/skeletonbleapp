package com.artjoker.data

import com.artjoker.core.gateways.NetworkFacade
import com.artjoker.data.internal.Api
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NetworkFacadeImpl @Inject constructor(private val api: Api) : NetworkFacade {
}
