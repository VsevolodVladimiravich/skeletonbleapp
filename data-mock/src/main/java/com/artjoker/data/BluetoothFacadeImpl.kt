package com.artjoker.data

import com.artjoker.core.entity.BluetoothDevice
import com.artjoker.core.entity.Commands
import com.artjoker.core.gateways.BluetoothFacade
import com.artjoker.core.interactors.BluetoothUpdate
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class BluetoothFacadeImpl @Inject constructor() : BluetoothFacade {
    override val enabled = true

    override fun startScan(): Observable<BluetoothUpdate> = Observable.fromIterable(mockScanResults())

    override fun connect(macAddress: String): Observable<BluetoothUpdate> = Observable.just(BluetoothUpdate.Connected)

    override fun readCharacteristics(): Observable<BluetoothUpdate.Characteristics> = Observable.just(BluetoothUpdate.Characteristics())

    override fun reconnect(): Observable<BluetoothUpdate> = Observable.just(BluetoothUpdate.Characteristics())

    override fun enableDevice(): Observable<BluetoothUpdate.Characteristics> = Observable.just(BluetoothUpdate.Characteristics())

    override fun disableDevice(): Observable<BluetoothUpdate.Characteristics> = Observable.just(BluetoothUpdate.Characteristics())

    override fun enableFirmwareState(): Observable<BluetoothUpdate> = Observable.just(BluetoothUpdate.FirmwareState)

    override fun setUpNotifications(): Observable<Observable<ByteArray>> = Observable.just(Observable.just(byteArrayOf()))

    override fun sendReady(): Single<String> = Single.just(Commands.RESPONSE.ACK)

    override fun sendDelete(): Single<String> = Single.just(Commands.RESPONSE.ACK)

    override fun uploadFirmware(firmware: List<ByteArray>) = Observable.just(BluetoothUpdate.UploadingFirmware(30, false))

    override fun sendEnd() = Observable.just(Commands.RESPONSE.ACK)

    private fun mockScanResults(): List<BluetoothUpdate.ScanResult> {
        val result = mutableListOf<BluetoothUpdate.ScanResult>()
        for (i in 1..5) {
            result.add(BluetoothUpdate.ScanResult(BluetoothDevice("name$i", "macAddress$i")))
        }
        return result
    }
}