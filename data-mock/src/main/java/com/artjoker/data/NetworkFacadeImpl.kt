package com.artjoker.data

import com.artjoker.core.gateways.NetworkFacade
import io.reactivex.Observable
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NetworkFacadeImpl @Inject constructor() : NetworkFacade {

    private val random = Random()

    private fun <T> justDelayed(item: T, delaySeconds: Long = 3) =
            Observable.interval(delaySeconds, TimeUnit.SECONDS)
                    .firstOrError()
                    .map { item }
}
