package com.artjoker.core.entity

data class BluetoothDevice constructor(
        val name: String?,
        val macAddress: String
)