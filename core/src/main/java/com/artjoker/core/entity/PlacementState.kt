package com.artjoker.core.entity

object PlacementState {
    const val BROWSE_AIR = 0
    const val DEFINITION_SURFACE = 1
    const val BROWSE_HAND = 2
    const val NOT_A_HAND = 3
    const val CHARGING = 4
    const val CANT_DEFINE_HART_RATE = 5
    const val TOO_ACTIVE = 6
    const val RECEIVING_INFO = -1
    const val OVERLOADED = 255
}