package com.artjoker.core.entity

class ManufacturerData constructor(bytes: ByteArray) {
    val deviceType: String = if (bytes[0].toInt() == 1) TYPE_BRACELET else TYPE_CHARGER
    val manufactureNumber = bytes[8].toInt()

    override fun toString(): String = "Device type: $deviceType," +
            " Manufacture number: $manufactureNumber"

    companion object {
        const val TYPE_BRACELET = "Bracelet"
        const val TYPE_CHARGER = "Charger"
    }
}