package com.artjoker.core.entity

object EnabledState{
    const val ENABLE = 85
    const val DISABLE = 51
    const val RECEIVING_INFO = -1
}