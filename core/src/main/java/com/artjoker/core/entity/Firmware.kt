package com.artjoker.core.entity

enum class Firmware(val fileName: String) {
    FIRMWARE_1("firmware_ 2.8.36.fwx"),
    FIRMWARE_2("firmware_ 2.8.37.fwx"),
    FIRMWARE_3("firmware_ 2.9.4.fwx"),
}