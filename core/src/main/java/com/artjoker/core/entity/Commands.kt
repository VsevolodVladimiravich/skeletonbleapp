package com.artjoker.core.entity

object Commands {


    object TX {

        val READY = byteArrayOf(0xF1.toByte(), 0xAF.toByte(), 0x1F)

        val DELETE = byteArrayOf(0xF1.toByte(), 0xDD.toByte(), 0x1F)

        val END = byteArrayOf(0xF1.toByte(), 0x0F, 0x1F)
    }


    object RX {

        val ACK = byteArrayOf(0xF1.toByte(), 0xAA.toByte(), 0x1F)

        val NACK = byteArrayOf(0xF1.toByte(), 0xBB.toByte(), 0x1F)
    }


    object REBOOT {

        val FIRMWARE_UPDATE: ByteArray = byteArrayOf(0xAD.toByte(), 0x10, 0x1E, 0x0B)
    }


    object SWITCH {

        val ON: ByteArray = byteArrayOf(0x55)

        val OFF: ByteArray = byteArrayOf(0x33)
    }


    object RESPONSE {

        val ACK = "ACK"

        val NACK = "NACK"
    }
}