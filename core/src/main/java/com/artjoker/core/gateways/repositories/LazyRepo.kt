package com.artjoker.core.gateways.repositories

import com.artjoker.core.gateways.LocalStorage
import com.artjoker.core.gateways.NetworkFacade
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject
import kotlin.reflect.KMutableProperty1

enum class ReloadCriteria { IF_NOT_CACHED, FORCED }


sealed class LazyRepo<T : Any>(
        private val cache: KMutableProperty1<LocalStorage, LocalStorage.ExpirableValue<T>>,
        private val update: (NetworkFacade) -> Single<T>) {

    @Inject lateinit var localStorage: LocalStorage
    @Inject lateinit var networkFacade: NetworkFacade

    fun get(criteria: ReloadCriteria): Observable<T> =
            cache.get(localStorage).let { cached ->
                when {
                    criteria == ReloadCriteria.FORCED || cached.expired ->
                        update(networkFacade)
                                .doOnSuccess { cache.set(localStorage, LocalStorage.ExpirableValue(it)) }
                                .toObservable()
                                .let { observable ->
                                    when {
                                        cached.value == null -> observable
                                        else -> observable.startWith(cached.value)
                                    }
                                }

                    else -> Observable.just(cached.value)
                }
            }
}
