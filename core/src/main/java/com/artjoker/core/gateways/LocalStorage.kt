package com.artjoker.core.gateways

interface LocalStorage {


    data class ExpirableValue<T : Any>(
            val value: T?,
            val expired: Boolean = true
    )
}
