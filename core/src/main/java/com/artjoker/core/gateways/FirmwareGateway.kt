package com.artjoker.core.gateways

import com.artjoker.core.entity.Firmware
import io.reactivex.Single

interface FirmwareGateway {
    fun getFirmware(firmware: Firmware): Single<List<ByteArray>>
}