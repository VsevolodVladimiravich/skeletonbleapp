package com.artjoker.core.gateways

import com.artjoker.core.interactors.BluetoothUpdate
import io.reactivex.Observable
import io.reactivex.Single

interface BluetoothFacade {

    val enabled: Boolean

    fun startScan(): Observable<BluetoothUpdate>

    fun connect(macAddress: String): Observable<BluetoothUpdate>

    fun readCharacteristics(): Observable<BluetoothUpdate.Characteristics>

    fun reconnect(): Observable<BluetoothUpdate>

    fun enableDevice(): Observable<BluetoothUpdate.Characteristics>

    fun disableDevice(): Observable<BluetoothUpdate.Characteristics>

    fun enableFirmwareState(): Observable<BluetoothUpdate>

    fun setUpNotifications(): Observable<Observable<ByteArray>>

    fun sendReady(): Single<String>

    fun sendDelete(): Single<String>

    fun uploadFirmware(firmware: List<ByteArray>): Observable<BluetoothUpdate.UploadingFirmware>

    fun sendEnd(): Observable<String>
}