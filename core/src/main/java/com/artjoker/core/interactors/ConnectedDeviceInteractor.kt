package com.artjoker.core.interactors

import com.artjoker.core.entity.*
import com.artjoker.core.gateways.BluetoothFacade
import com.artjoker.core.gateways.FirmwareGateway
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ConnectedDeviceInteractor @Inject constructor(
        private val facade: BluetoothFacade,
        private val gateway: FirmwareGateway
) {

    fun stateChanges(supplier: NativeDeviceUpdates): Observable<DeviceState> = supplier.updates()
            .switchMap {
                when (it) {
                    is NativeDeviceUpdate.PermissionNotGranted -> Observable.just(DeviceState.NoPermissions)
                    is NativeDeviceUpdate.PermissionGranted -> processPermissionGranted()
                    is NativeDeviceUpdate.BluetoothEnabled -> Observable.just(DeviceState.ReadyToScan)

                    is NativeDeviceUpdate.StartScan -> processScanUpdates()
                    is NativeDeviceUpdate.Connect -> connect(it.macAddress)
                    is NativeDeviceUpdate.Reconnect -> processScanUpdates()

                    is NativeDeviceUpdate.EnableDevice -> processSwitchDeviceState(true)
                    is NativeDeviceUpdate.DisableDevice -> processSwitchDeviceState(false)

                    is NativeDeviceUpdate.PrepareFirmwareUpdate -> processPrepareFirmwareUpdate()
                    is NativeDeviceUpdate.ReadyToUpdate -> processFirmwareUpdate(it.firmware)
                    is NativeDeviceUpdate.FinishUpdate -> processSendEnd()
                }
            }

    private fun connect(macAddress: String): Observable<DeviceState> = facade.connect(macAddress)
            .startWith(BluetoothUpdate.Connecting(macAddress))
            .flatMap { processConnection(it) }

    private fun processConnection(update: BluetoothUpdate): Observable<DeviceState> =
            when (update) {
                is BluetoothUpdate.Connecting -> Observable.just(DeviceState.Connecting(update.macAddress))
                is BluetoothUpdate.Connected -> readCharacteristics().startWith(DeviceState.Connected())
                is BluetoothUpdate.Disconnected -> Observable.just(DeviceState.Disconnected)
                else -> error(update)
            }

    private fun processPermissionGranted(): Observable<DeviceState> = when {
        facade.enabled -> DeviceState.ReadyToScan
        else -> DeviceState.BluetoothDisabled
    }.let { Observable.just(it) }

    private fun processScanUpdates(): Observable<DeviceState> = facade.startScan()
            .startWith(BluetoothUpdate.Scanning)
            .switchMap {
                when (it) {
                    is BluetoothUpdate.Scanning -> Observable.just(DeviceState.Scanning())
                    is BluetoothUpdate.ScanResult -> Observable.just(DeviceState.Scanning(it.device))
                    else -> error(it)
                }
            }

    private fun processSwitchDeviceState(enable: Boolean): Observable<DeviceState> = when (enable) {
        true -> facade.enableDevice()
        else -> facade.disableDevice()
    }.map { DeviceState.Connected(it.deviceEnabled, it.placementState, it.firmware) }

    private fun readCharacteristics(): Observable<DeviceState> = facade.readCharacteristics()
            .map { DeviceState.Connected(it.deviceEnabled, it.placementState, it.firmware) }

    private fun processPrepareFirmwareUpdate(): Observable<DeviceState> =
            facade.enableFirmwareState()
                    .concatMap {
                        when (it) {
                            is BluetoothUpdate.FirmwareState -> Observable.just(DeviceState.ReadyToUpdate)
                            else -> error(it)
                        }
                    }

    private fun processFirmwareUpdate(firmware: Firmware): Observable<DeviceState> = facade.reconnect()
            .concatMap { facade.setUpNotifications() }
            .flatMap { processSendReady(firmware) }
            .startWith(DeviceState.UploadingFirmware())

    private fun processSendReady(firmware: Firmware): Observable<DeviceState> = processSendCommand(
            facade.sendReady(),
            processSendDelete(firmware)
                    .startWith(DeviceState.UploadingFirmware(UploadCompletedPercent.DEVICE_RECONNECTED)))

    private fun processSendDelete(firmware: Firmware): Observable<DeviceState> = processSendCommand(
            facade.sendDelete(),
            processFirmwareUpload(firmware)
                    .startWith(DeviceState.UploadingFirmware(UploadCompletedPercent.DELETE_SENT)))

    private fun processSendEnd(): Observable<DeviceState> = processSendCommand(
            facade.sendEnd(),
            Observable.just<DeviceState>(DeviceState.FirmwareUploadSuccess))

    private fun processSendCommand(command: Single<String>, observable: Observable<DeviceState>):
            Observable<DeviceState> = processSendCommand(command.toObservable(), observable)

    private fun processSendCommand(command: Observable<String>, observable: Observable<DeviceState>):
            Observable<DeviceState> = command.concatMap {
        when (it.also(::println)) {
            Commands.RESPONSE.ACK -> observable
            else -> Observable.just(DeviceState.FirmwareUploadFail)
        }
    }

    private fun processFirmwareUpload(firmware: Firmware): Observable<DeviceState> =
            gateway.getFirmware(firmware)
                    .flatMapObservable(facade::uploadFirmware)
                    .switchMap {
                        when (it.isLast) {
                            true -> DeviceState.UploadingFinished(it.progress)
                            else -> DeviceState.UploadingFirmware(it.progress)
                        }.let { Observable.just(it) }
                    }

    private fun <T> error(it: Any): Observable<T> =
            Observable.error(UnsupportedOperationException(it.toString()))
}


interface NativeDeviceUpdates {

    fun updates(): Observable<NativeDeviceUpdate>
}


object UploadCompletedPercent {
    const val DEVICE_RECONNECTED = 15
    const val DELETE_SENT = 30
}


sealed class NativeDeviceUpdate {


    object PermissionGranted : NativeDeviceUpdate()


    object PermissionNotGranted : NativeDeviceUpdate()


    object BluetoothEnabled : NativeDeviceUpdate()


    object StartScan : NativeDeviceUpdate()


    data class Connect constructor(
            val macAddress: String
    ) : NativeDeviceUpdate()


    object PrepareFirmwareUpdate : NativeDeviceUpdate()


    object EnableDevice : NativeDeviceUpdate()


    object DisableDevice : NativeDeviceUpdate()


    object Reconnect : NativeDeviceUpdate()


    object FinishUpdate : NativeDeviceUpdate()


    data class ReadyToUpdate constructor(
            val firmware: Firmware
    ) : NativeDeviceUpdate()
}


sealed class BluetoothUpdate {


    object Scanning : BluetoothUpdate()


    data class ScanResult constructor(
            val device: BluetoothDevice
    ) : BluetoothUpdate()


    data class Connecting constructor(
            val macAddress: String = ""
    ) : BluetoothUpdate()


    object Connected : BluetoothUpdate()


    object Disconnected : BluetoothUpdate()


    data class Characteristics constructor(
            val deviceEnabled: Int = EnabledState.ENABLE,
            val placementState: Int = PlacementState.RECEIVING_INFO,
            val firmware: Int = FirmwareType.RECEIVING_INFO
    ) : BluetoothUpdate()


    object FirmwareState : BluetoothUpdate()


    data class UploadingFirmware constructor(
            val progress: Int,
            val isLast: Boolean
    ) : BluetoothUpdate()
}


sealed class DeviceState {

    val name: String = this.javaClass.name
    open val toggleTurningAvailable = false
    open val loadPercentage = 0


    object None : DeviceState()


    object NoPermissions : DeviceState()


    object BluetoothDisabled : DeviceState()


    object ReadyToScan : DeviceState()


    data class Scanning constructor(
            val device: BluetoothDevice? = null
    ) : DeviceState()


    data class Connecting constructor(
            val macAddress: String
    ) : DeviceState()


    data class Connected constructor(
            val deviceEnabled: Int = EnabledState.RECEIVING_INFO,
            val placementState: Int = PlacementState.RECEIVING_INFO,
            val firmware: Int = FirmwareType.RECEIVING_INFO
    ) : DeviceState() {

        override val toggleTurningAvailable
            get() = deviceEnabled == EnabledState.ENABLE
                    || deviceEnabled == EnabledState.RECEIVING_INFO
    }


    object Disconnected : DeviceState()


    object ReadyToUpdate : DeviceState()


    data class UploadingFirmware constructor(
            override val loadPercentage: Int = 0
    ) : DeviceState()


    object CancellingUploadingFirmware : DeviceState()


    object FirmwareUploadSuccess : DeviceState()


    object FirmwareUploadFail : DeviceState()


    data class UploadingFinished constructor(
            override val loadPercentage: Int = 0
    ) : DeviceState()

}
